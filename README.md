[TOC]

# configure_etc - ansible role for configuring linux /etc/ settings

## current funcionality

1. [/etc/hostname](https://man7.org/linux/man-pages/man5/hostname.5.html) via [ansible.builtin.hostname](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/hostname_module.html) module
1. [/etc/hosts](https://man7.org/linux/man-pages/man5/hosts.5.html) for localhost via [ansible.builtin.lineinfile](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/lineinfile_module.html)
1. [/etc/hosts](https://man7.org/linux/man-pages/man5/hosts.5.html) for groups of hosts via [ansible.builtin.blockinfile](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/blockinfile_module.html)
